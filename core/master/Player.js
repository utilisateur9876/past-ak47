"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Player = void 0;
var Player = /** @class */ (function () {
    function Player(id, coins, username) {
        this.id = id;
        this.username = username;
        this.coins = coins;
    }
    return Player;
}());
exports.Player = Player;
