export declare type BotConfig = {
    token: string;
    prefix: string;
    enableReactions: boolean;
};
export declare let config: BotConfig;
