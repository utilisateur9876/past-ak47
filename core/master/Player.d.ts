declare class Player {
    id: number;
    coins: number;
    username: string;
    constructor(id: number, coins: number, username: string);
}
export { Player };
