"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
// Extract the required classes from the discord.js module
var discord_js_1 = require("discord.js");
// Load modules
var config_1 = require("./master/config");
var Player_1 = require("./master/Player");
/******************************************************************************
 // Récupération des arguments du promp et déclaration
 *****************************************************************************/
var argument = process.argv.slice(2);
var date = new Date();
var client = new discord_js_1.Client();
config_1.config.prefix = "digitall:";
config_1.config.token = argument[0];
// These are the arguments behind the commands.
var victory;
var listPlayer = [];
/******************************************************************************
 // Utilisation du client
 *****************************************************************************/
// tslint:disable-next-line:no-console
client.on("ready", function () {
    console.log(" _____   _       _           _ _   _______      ");
    console.log("(____ \ (_)     (_)_        | | | (_______)        ");
    console.log(" _   \ \ _  ____ _| |_  ____| | |    __    ___  ____   ____ ");
    console.log("| |   | | |/ _  | |  _)/ _  | | |   / /   / _ \|  _ \ / _  )");
    console.log("| |__/ /| ( ( | | | |_( ( | | | |_ / /___| |_| | | | ( (/ / ");
    console.log("|_____/ |_|\_|| |_|\___)_||_|_|_(_|_______)___/|_| |_|\____)");
    console.log("          (_____|                                           ");
    console.log("");
    console.log("@Author : Jean-Christophe H");
    console.log("@Project: https://gitlab.com/jc.henry/");
    console.log("");
    console.log("Script post install ubuntu desktop or server");
    console.log("");
    console.log("Copyright (C) 2020");
    console.log("This program is free software; you can redistribute it and/or modify");
    console.log("it under the terms of the GNU General Public License as published by");
    console.log("the Free Software Foundation; either version 3 of the License, or");
    console.log("(at your option) any later version.");
    console.log("");
    console.log("This program is distributed in the hope that it will be useful,");
    console.log("but WITHOUT ANY WARRANTY; without even the implied warranty of");
    console.log("MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the");
    console.log("GNU General Public License for more details.");
    console.log("");
    console.log("---------------------------------");
    console.log("Le bot de combat RP a démarré !");
    console.log("---------------------------------");
});
// tslint:disable-next-line:no-console
client.on("error", function (e) { return console.error("Discord error!", e); });
client.on("message", function (message) { return __awaiter(void 0, void 0, void 0, function () {
    var command, _loop_1, _i, listPlayer_1, item;
    return __generator(this, function (_a) {
        command = message.content.toLowerCase().slice(config_1.config.prefix.length).split(" ")[0];
        // If the message does not start with your prefix return.
        // If the user that types a message is a bot account return.
        // If the command comes from DM return.
        if (!message.content.startsWith(config_1.config.prefix) || message.author.bot || !message.guild)
            return [2 /*return*/];
        // Utilisation d'un switchcase pour facilité l'utilisation des commandes.
        switch (command) {
            case "fight": {
                // Check member permissions
                /*if (!message.member.hasPermission("ADMINISTRATOR")) {
                    // You must be an administrator of this server to request a backup!
                    return message.channel.send(":x: | Vous devez être administrateur de ce serveur pour utiliser le bot !");
                }*/
                console.log("Une partie à était lancé par : ", message.author);
                console.log("------------------------------");
                console.log("");
                // Sysstème de randome de victoire
                victory = Math.random() >= 0.5;
                // Si c'est la première fois que le bot démarre on ajoute un joueur
                if (listPlayer.length === 0) {
                    listPlayer.push(new Player_1.Player(Number(message.author.id), 20, message.author.username));
                }
                _loop_1 = function (item) {
                    if (item.id !== Number(message.author.id)) {
                        listPlayer.push(new Player_1.Player(Number(message.author.id), 20, message.author.username));
                    }
                    else {
                        message.channel.send(":white_check_mark: " + message.author.username + "  VS  Arthur de Camelot ! ");
                        message.channel.send("-----------------------------------");
                        message.channel.send(":three: :two: :one: :crossed_swords:");
                        setTimeout(function () {
                            if (victory) {
                                item.coins = item.coins + 20;
                                message.channel.send(item.username + "   A GAGNER ! Bien jouer soldat !");
                                return message.channel.send("Il te reste : " + item.coins + " pièces");
                            }
                            else {
                                item.coins = item.coins - 10;
                                message.channel.send("Arthur de Camelot A GAGNER ! Tu feras mieux la prochaine fois.");
                                return message.channel.send("Il te reste : " + item.coins + " pièces");
                            }
                        }, 300 / 60);
                    }
                };
                for (_i = 0, listPlayer_1 = listPlayer; _i < listPlayer_1.length; _i++) {
                    item = listPlayer_1[_i];
                    _loop_1(item);
                }
                break;
            }
            default: {
                return [2 /*return*/, message.channel.send(":x: | La commande n'existe pas'`!")];
            }
        }
        return [2 /*return*/];
    });
}); });
/******************************************************************************
 // Connexion à l'API de Discord JS
 *****************************************************************************/
// Your secret token to log the bot in. (never share this to anyone!)
client.login(config_1.config.token);
