class Player {
    id: number;
    coins: number;
    username: string;

    constructor(id: number, coins: number, username: string) {
        this.id = id;
        this.username = username;
        this.coins = coins;
    }
}

export { Player }