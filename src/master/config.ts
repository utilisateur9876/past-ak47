export type BotConfig = {
    token: string;
    prefix: string;
    enableReactions: boolean;
}

export let config: BotConfig = {
    token: "",
    prefix: "./",
    enableReactions: true
}