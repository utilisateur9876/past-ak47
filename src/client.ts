// Extract the required classes from the discord.js module
import {Client} from "discord.js";
// Load modules
import {config} from "./master/config";
import {Player} from './master/Player';

/******************************************************************************
 // Récupération des arguments du promp et déclaration
 *****************************************************************************/
const argument = process.argv.slice(2);
const date: Date = new Date();

const client = new Client();
config.prefix = "digitall:";
config.token = argument[0];
// These are the arguments behind the commands.
let victory: boolean
const listPlayer: Player[] = [];

/******************************************************************************
 // Utilisation du client
 *****************************************************************************/
// tslint:disable-next-line:no-console
client.on("ready", () => {
    console.log(" _____   _       _           _ _   _______      ");
    console.log("(____ \ (_)     (_)_        | | | (_______)        ");
    console.log(" _   \ \ _  ____ _| |_  ____| | |    __    ___  ____   ____ ");
    console.log("| |   | | |/ _  | |  _)/ _  | | |   / /   / _ \|  _ \ / _  )");
    console.log("| |__/ /| ( ( | | | |_( ( | | | |_ / /___| |_| | | | ( (/ / ");
    console.log("|_____/ |_|\_|| |_|\___)_||_|_|_(_|_______)___/|_| |_|\____)");
    console.log("          (_____|                                           ");
    console.log("");
    console.log("@Author : Jean-Christophe H");
    console.log("@Project: https://gitlab.com/jc.h/");
    console.log("");
    console.log("Script post install ubuntu desktop or server");
    console.log("");
    console.log("Copyright (C) 2020");
    console.log("This program is free software; you can redistribute it and/or modify");
    console.log("it under the terms of the GNU General Public License as published by");
    console.log("the Free Software Foundation; either version 3 of the License, or");
    console.log("(at your option) any later version.");
    console.log("");
    console.log("This program is distributed in the hope that it will be useful,");
    console.log("but WITHOUT ANY WARRANTY; without even the implied warranty of");
    console.log("MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the");
    console.log("GNU General Public License for more details.");
    console.log("");
    console.log("---------------------------------");
    console.log("Le bot de combat RP a démarré !");
    console.log("---------------------------------");
});
// tslint:disable-next-line:no-console
client.on("error", e => console.error("Discord error!", e));

client.on("message", async (message: any) => {

    // This reads the first part of your message behind your prefix to see which command you want to use.
    const command = message.content.toLowerCase().slice(config.prefix.length).split(" ")[0];

    // If the message does not start with your prefix return.
    // If the user that types a message is a bot account return.
    // If the command comes from DM return.
    if (!message.content.startsWith(config.prefix) || message.author.bot || !message.guild) return;

    // Utilisation d'un switchcase pour facilité l'utilisation des commandes.
    switch (command) {
        case "fight": {
            // Check member permissions
            /*if (!message.member.hasPermission("ADMINISTRATOR")) {
                // You must be an administrator of this server to request a backup!
                return message.channel.send(":x: | Vous devez être administrateur de ce serveur pour utiliser le bot !");
            }*/

            console.log("Une partie à était lancé par : ", message.author);
            console.log("------------------------------");
            console.log("");

            // Sysstème de randome de victoire
            victory = Math.random() >= 0.5;

            // Si c'est la première fois que le bot démarre on ajoute un joueur
            if (listPlayer.length === 0) {
                listPlayer.push(new Player(Number(message.author.id), 20, message.author.username));
            }

            for (const item of listPlayer) {
                if (item.id !== Number(message.author.id)) {
                    listPlayer.push(new Player(Number(message.author.id), 20, message.author.username));
                } else {

                    message.channel.send(":white_check_mark: " + message.author.username + "  VS  Arthur de Camelot ! ");
                    message.channel.send("-----------------------------------");
                    message.channel.send(":three: :two: :one: :crossed_swords:");

                    setTimeout(() => {
                        if (victory) {
                            item.coins = item.coins + 20;
                            message.channel.send(item.username + "   A GAGNER ! Bien jouer soldat !");
                            return message.channel.send("Il te reste : " + item.coins + " pièces");
                        } else {
                            item.coins = item.coins - 10;
                            message.channel.send("Arthur de Camelot A GAGNER ! Tu feras mieux la prochaine fois.");
                            return message.channel.send("Il te reste : " + item.coins+ " pièces");
                        }
                    }, 300/60);
                }
            }
            break;

        }
        default: {
            return message.channel.send(":x: | La commande n'existe pas'`!");
        }
    }
});


/******************************************************************************
 // Connexion à l'API de Discord JS
 *****************************************************************************/
// Your secret token to log the bot in. (never share this to anyone!)
client.login(config.token);
