# Discord fight bot rp


[![Démo](https://gitlab.com/jc.henry/past-ak47/-/raw/master/demo.png?inline=false)](https://gitlab.com/jc.henry/past-ak47/-/raw/master/demo.png?inline=false)

## Pour voir les détail qualité cliqué le bouton :

[![Quality gate](https://sonarcloud.io/api/project_badges/quality_gate?project=jc.henry_past-ak47)](https://sonarcloud.io/dashboard?id=jc.henry_past-ak47)

**Note**: Le module est compatible avec discord.js version 12.

Fight bot est un module [Node.js](https://nodejs.org)

Le but est d'avoir un bot RPG qui lance un combat avec l'auteur de la commande, puis donne des points : 

```
digitall:fight
```

## Changelog

* Première version avec le minimum requis.

## link for add bot

https://discord.com/oauth2/authorize?client_id=727301150419976212&scope=bot&permissions=8

## install project

Cloner le dépôt sur l'endroit de votre choix, ensuite aller dans le dossier tu projet.

```bash
yarn install
```
ou 
```bash
npm install
```

## run project

On build le projet avec yarn 
```bash
yarn build 
```

ou avec NPM 
```bash
npm run build 
```

Puis on éxécute le serveur avec yarn
```bash
yarn start tokenBot
```

ou avec NPM
Avec NPM 
```bash
npm run start tokenBot
```

## Développement

Pré-requis : nodeJS

Langague utilisé pour le développement : 

[![Démo](https://blog.engineering.publicissapient.fr/wp-content/uploads/2014/03/typescript-logo.png)](https://blog.engineering.publicissapient.fr/wp-content/uploads/2014/03/typescript-logo.png)

Tout ce passe dans le fichier server.ts : https://gitlab.com/jc.henry/past-ak47/-/blob/master/src/server.ts
